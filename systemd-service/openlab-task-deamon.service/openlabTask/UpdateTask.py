#!/usr/bin/env python3
#-*- coding:utf-8 -*-

import time

from openlabTask.TaskCommon import TaskCommon
from openlabTask.WebInteract import WebInteract


class UpdateTask(TaskCommon):
        """
        UpdateTask Class

        1. Process task which status is os_installing
                                     or os_started
                                     or sshd_started
                                     or expected_os
                                     or new_os_installed
        2: when os_installing, SSH test
           if OK, set status to be os_started
        3: when os_started, check OS
           if OK, set status to be expected_os
           or not, set to be unexpected_os
        4. when expected_os, check last login record
           if OK, set status to be new_os_installed
           or not, set to be old_os_restarted
        5. when new_os_installed, post task result
           if OK, set status to be allocated
           or not, set to be allocate_fail
        6. when result is Success or Fail, update servers file
        """

        def __init__(self):
                super().__init__()
                self.__update_result()

        def __calc_cost(self, start, end):
                cmd = "echo $(($(date +%s -d'{}') - $(date +%s -d'{}')))".format(end, start)
                res = int(self.execute_cmd(cmd))
                min, sec = res // 60, res % 60
                return '%s min(s) %s sec(s)' % (min, sec)

        def __update_result(self):
                update_servers = False
                servers_info = self.read_yaml(self.servers_file)
                for task in self.task_list:
                        system_task_info = self.read_yaml(task)
                        if not system_task_info:
                                continue
                        if 'vmBase' in system_task_info:
                                continue
                        host = system_task_info['hostName']
                        bmc_host = system_task_info['bmcHostName']
                        status = system_task_info['status']
                        update_info, finish = True, False
                        if status == 'os_installing':
                                result = self.execute_cmd('ssh -q -o StrictHostKeyChecking=no %s echo os_started' % host)
                                if 'os_started' in result:
                                        self.log('UpdateTask', 'Test SSH %s return:\n%s' % (host, result))
                                        system_task_info['status'] = 'os_started'
                        elif status == 'os_started':
                                result = self.execute_cmd("ssh -q -o StrictHostKeyChecking=no %s \
                                                           'cat /etc/system-release && arch'" % host)
                                if 'No such file' in result:
                                        # Debian, fall back to os-release
                                        self.log('UpdateTask', 'Fall back to check os-release')
                                        result = self.execute_cmd("ssh -q -o StrictHostKeyChecking=no %s \
                                                                   'cat /etc/os-release && arch'" % host)
                                self.log('UpdateTask', 'Check %s OS return:\n%s' % (host, result))
                                update_os = True
                                for item in system_task_info['expectedOS'].split(' '):
                                        if item not in result:
                                                update_os = False
                                                break
                                if update_os:
                                        system_task_info['status'] = 'expected_os'
                                else:
                                        system_task_info['status'] = 'unexpected_os'
                                        system_task_info['result'] = 'Fail'
                                        system_task_info['msg'] = '环境发放失败'
                                        finish = True
                        elif status == 'expected_os':
                                cmd1, cmd2 = "ssh -q -o StrictHostKeyChecking=no %s \
                                     \"last | grep 'system boot' | tail -n1 | awk \'{print \$5,\$6,\$7,\$8}\'\"" % host,\
                                             "ssh -q -o StrictHostKeyChecking=no {} \
                                     \"env LANG=en_US.UTF-8 date +\'%a %b %-d %H:%M\'\"".format(host)
                                result1, result2= self.execute_cmd(cmd1), self.execute_cmd(cmd2)
                                self.log('UpdateTask', '%s last login record:\n%s' % (host, result1))
                                self.log('UpdateTask', '%s current date:\n%s' % (host, result2))
                                result1, result2 = int(self.execute_cmd('date -d "{}" +%s'.format(result1))),\
                                                   int(self.execute_cmd('date -d "{}" +%s'.format(result2)))
                                system_task_info['status'] = 'new_os_installed'
                        elif status == 'new_os_installed':
                                system_task_info['result'] = 'Success'
                                if system_task_info['operation'] == 'CREATE':
                                        root_passwd = system_task_info['rootPassword']
                                        result = self.execute_cmd("ssh -q -o StrictHostKeyChecking=no %s \
                                                                   'echo \"%s\" | passwd --stdin root'" % (host, root_passwd))
                                        if 'unrecognized option' in result:
                                                # Debian, use chpasswd
                                                self.log('UpdateTask', 'Use chpasswd instead')
                                                result = self.execute_cmd("ssh -q -o StrictHostKeyChecking=no %s \
                                                                           'echo \"root:%s\" | chpasswd'" % (host, root_passwd))
                                        self.log('UpdateTask', 'Change %s root password return:\n%s' % (host, result))
                                        system_task_info['status'] = 'allocated'
                                        system_task_info['msg'] = '环境发放成功'
                                        # servers_info[bmc_host]['User'] = system_task_info['applicant']
                                else:
                                        root_passwd = self.execute_cmd('openssl rand -base64 32 | tr A-Z a-z | cut -c 1-10').split('\n')[0]
                                        result = self.execute_cmd("ssh -q -o StrictHostKeyChecking=no %s \
                                                                   'echo '%s' | passwd --stdin root'" % (host, root_passwd))
                                        self.log('UpdateTask', 'Change %s root password return:\n%s' % (host, result))
                                        system_task_info['status'] = 'recycled'
                                        system_task_info['msg'] = '环境回收成功'
                                        # servers_info[bmc_host]['User'] = ''
                                result = WebInteract(self.web_key).post_result(system_task_info)
                                self.log('UpdateTask', 'Post %s task result return:\n%s' % (host, result))
                                finish, update_servers = True, True
                        else:
                                update_info = False
                        if finish:
                                system_task_info['endTime'] = str(time.strftime('%Y-%m-%d %H:%M:%S', \
                                                                                time.localtime(time.time())))
                                system_task_info['costTime'] = self.__calc_cost(system_task_info['startTime'], \
                                                                                system_task_info['endTime'])
                        if update_info:
                                self.save_yaml(system_task_info, task)
                if update_servers:
                        self.save_yaml(servers_info, self.servers_file)
                        self.execute_cmd('cp -rp %s %s' % (self.servers_file, self.servers_rawfile))

