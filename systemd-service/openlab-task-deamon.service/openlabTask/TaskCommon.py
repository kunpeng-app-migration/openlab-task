#!/usr/bin/env python3
#-*- coding:utf-8 -*-
#
# preset file: 
#    1. '/srv/openlab-task/rnd-web-key'
#       from: https://gitee.com/kunpeng-app-migration/security/blob/master/openlab/rnd-web-key

import subprocess
import logging
import time
import yaml
import os


class TaskCommon:
        """
        TaskCommon Class

        1. Initialize task workspace
        2. Read task common config
        3. Define common functions
        """

        def __init__(self):
                self.hosts_file = '/etc/hosts'
                self.servers_rawfile = '/etc/servers'
                self.workspace = '/srv/openlab-task'
                self.tmp_file_path = '/tmp/openlab-task'
                self.web_key_file = '/srv/openlab-task/rnd-web-key' #preset file
                self.tbox_ssh_ports_file = os.path.join(self.workspace, 'tbox-ssh-ports')
                self.tbox_service_ports_file = os.path.join(self.workspace, 'tbox-service-ports')
                self.servers_file = os.path.join(self.workspace, 'servers')
                self.log_path = os.path.join(self.workspace, 'logs')
                self.task_path = os.path.join(self.workspace, 'tasks')
                self.vm_chassis_start = 80

                # init config
                self.__os_mapping()
                self.__zone_mapping()
                self.__get_task()
                self.__get_web_key()

        def __os_mapping(self):
                self.support_os = {
                        'CentOS 7.6 for ARM': 'CentOS aarch64 7.6.1810',
                        'CentOS 7.7 for ARM': 'CentOS aarch64 7.7.1908',
                        'CentOS 7.8 for ARM': 'CentOS aarch64 7.8.2003',
                        'CentOS 8.1 for ARM': 'CentOS aarch64 8.1.1911',
                        'CentOS 8.2 for ARM': 'CentOS aarch64 8.2.2004',
                        'openEuler 20.03 LTS for ARM': 'openEuler aarch64 20.03 (LTS-SP1)',
                        'openEuler 20.03 LTS SP1 for ARM': 'openEuler aarch64 20.03 (LTS-SP1)',
                        'Ubuntu 18.04.1 LTS for ARM': 'Ubuntu aarch64 18.04.1 LTS',
                        'Kylin Linux Advanced Server V10 for ARM': 'Kylin V10 aarch64',
                        'UOS Server 20 Euler(1000) for ARM': 'UnionTech OS Server 20 Euler aarch64',
                        'UOS Server 20 Euler(1010) for ARM': 'UnionTech OS Server 20 Euler aarch64',
                        'NeoKylin Server V7.0 U6 for ARM': 'NeoKylin Linux Advanced Server V7.0',
                        'Ubuntu 18.04.4 LTS for ARM': 'Ubuntu aarch64 18.04.1 LTS',
                        'Ubuntu 20.04.1 LTS for ARM': 'Ubuntu aarch64 20.04.1 LTS'
                     }
                self.cblr_profile = {
                        'CentOS aarch64 7.6.1810': 'centos-7.6.1810-aarch64-aarch64',
                        'CentOS aarch64 7.7.1908': 'centos-7.7.1908-aarch64-aarch64',
                        'CentOS aarch64 7.8.2003': 'centos-7.8.2003-aarch64-aarch64',
                        'CentOS aarch64 8.1.1911': 'centos-8.1.1911-aarch64-aarch64',
                        'CentOS aarch64 8.2.2004': 'centos-8.2.2004-aarch64-aarch64',
                        'openEuler aarch64 20.03 (LTS-SP1)': 'openeuler-20.03-SP1-aarch64-aarch64',
                        'Ubuntu aarch64 18.04.1 LTS': 'ubuntu-18.04.1-test',
                        'Kylin V10 aarch64': 'kylin-10-sp1-aarch64-aarch64',
                        'UnionTech OS Server 20 Euler aarch64': 'uos-euler-V20-1010-aarch64-aarch64',
                        'NeoKylin Linux Advanced Server V7.0': 'neokylin-7.0-u6.aarch64-aarch64',
                        'Ubuntu aarch64 20.04.1 LTS': 'ubuntu-20.04.1-aarch64-arm64-aarch64'
                     }

        def __zone_mapping(self):
                self.support_zone = {
                        'BA1': ['Hangzhou-Z9', 'N']
                     }

        def __get_task(self):
                self.task_list = self.execute_cmd("find %s -type f | grep -v -E 'swp|.log|tbox|servers' | sort -r" \
                                                        % self.task_path).split('\n')

        def __get_web_key(self):
                self.web_key = self.execute_cmd('cat %s' % self.web_key_file).split('\n')[0]

        def execute_cmd(self, cmd):
            sub = subprocess.Popen(
                      args=cmd,
                      shell=True,
                      stdout=subprocess.PIPE,
                      stderr=subprocess.STDOUT
                     )
            sub.wait()
            return sub.stdout.read().decode(encoding='utf-8', errors='strict').strip()
        
        def log(self, module, msg):
                date = str(time.strftime('%Y%m%d', time.localtime(time.time())))
                fpath = os.path.join(self.log_path, date, 'all.log')
                self.execute_cmd('mkdir -p $(dirname %s)' % fpath)
                logging.basicConfig(filename=fpath,
                                    level=logging.INFO,
                                    format="[%(asctime)s] %(levelname)s: %(message)s")
                logging.info('[%s]: %s' % (module, msg))
        
        def read_yaml(self, dstfile):
                with open(dstfile, 'r', encoding='utf-8') as f:
                        return yaml.safe_load(f.read())

        def save_yaml(self, data, dstfile):
                with open(dstfile, 'w') as f:
                        yaml.dump(data, f, encoding="utf-8", allow_unicode=True)

