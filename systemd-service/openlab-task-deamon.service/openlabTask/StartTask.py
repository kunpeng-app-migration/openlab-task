#!/usr/bin/env python3
#-*- coding:utf-8 -*-

import time

from openlabTask.TaskCommon import TaskCommon


class StartTask(TaskCommon):
        """
        StartTask Class

        1. Process task status task_added
        2. Call install-os
        3. Set status to be os_installing
        """

        def __init__(self):
                super().__init__()
                self.__start_task()

        def __start_task(self):
                start = False
                for task in self.task_list:
                        system_task_info = self.read_yaml(task)
                        if not isinstance(system_task_info, dict):
                                continue
                        if 'vmBase' in system_task_info:
                                continue
                        if system_task_info['status'] == 'task_added':
                                host = system_task_info['hostName']
                                res = self.execute_cmd('install-os %s' % host)
                                self.log('StartTask', 'install-os %s return:\n%s' % (host, res))
                                system_task_info['status'] = 'os_installing'
                                system_task_info['startTime'] = str(time.strftime('%Y-%m-%d %H:%M:%S', \
                                                                                  time.localtime(time.time())))
                                self.save_yaml(system_task_info, task)
                                start = True
                if start:
                        time.sleep(30)

