#!/usr/bin/env python3
#-*- coding: utf-8 -*-

import time
import re
import requests
import json

from binascii import b2a_hex, a2b_hex
from Crypto.Cipher import AES


class WebInteract:

        def __init__(self, key):
                self.__KEY = key

        def __padding(self, _str):
                '''
                加密数据补位
                '''
                return _str + (16 - len(_str) % 16) * chr(16 - len(_str) % 16).encode('utf-8')
        
        def __un_padding(self, _str):
                '''
                解密数据去掉补充位
                '''
                return _str[0:-(_str[-1])]
        
        def __get_timestamp(self):
                return str(int(time.time() * 1000)).rjust(16, '0')
        
        def __encrypt(self, data, timestamp):
                '''
                加密数据，data位数必须是16倍数，否则需要填充，输出结果16进制字符串
                '''
                cipher = AES.new(self.__KEY.encode('utf-8'), AES.MODE_CBC,
                                 timestamp.encode('utf-8'))
                result = cipher.encrypt(self.__padding(data.encode('utf-8')))
                return b2a_hex(result)
        
        def __decrypt(self, data, timestamp):
                '''
                解密数据，data后面填充位需要补充，输入内容为16进制字符串
                '''
                cipher = AES.new(self.__KEY.encode('utf-8'), AES.MODE_CBC,
                                 timestamp.encode('utf-8'))
                result = cipher.decrypt(a2b_hex(data))
                return self.__un_padding(result).decode('utf-8')
        
        def __get_token(self, s):
                '''
                获取网站访问CSRF Token
                '''
                url = 'https://ic-openlabs.huawei.com/openlab/'
                headers = {
                        'Referer': 'https://ic-openlabs.huawei.com/'
                }
                resp = s.get(url, headers=headers)
                head = resp.headers
                cookie = head.get('Set-Cookie', "")
                csrf = re.findall(r"CSRF-TOKEN=(.+?)[;']", cookie)[0]
                return csrf

        def get_list(self):
                '''
                请求任务列表
                '''
                s = requests.session()
                csrf = self.__get_token(s)
                timestamp = self.__get_timestamp()
                url = 'https://ic-openlabs.huawei.com/openlab/api/v1/blue-area-server/pm/list?timestamp=' + timestamp
                headers = {
                        'Referer': 'https://ic-openlabs.huawei.com/',
                        'X-CSRF-TOKEN': csrf,
                        'Content-Type': 'application/json'
                }
                resp = s.post(url, data=self.__encrypt("", timestamp), headers=headers)
                if resp.status_code == 200:
                        resp_body = json.loads(self.__decrypt(resp.text, timestamp))
                        if resp_body.get('code', None) != '0000':
                                msg = resp_body
                        else:
                                msg = resp_body.get('data', [])
                else:
                        msg = [resp.status_code, str(resp.text)]
                s.close()
                return msg
        
        def post_result(self, result):
                '''
                result: yaml data
                result_list: json data. '{"list": [json.dumps(result)]}'
                向网站返回结果: result_list = 
                       '{"list":
                                [
                                 {
                                   "serverName": "DC1_11_010",
                                   "serverIp": "192.168.195.253",
                                   "result": "Success",
                                   "msg": "环境发放成功"
                                  }
                                 ]
                        }'
                '''
                s = requests.session()
                csrf = self.__get_token(s)
                timestamp = self.__get_timestamp()
                url = 'https://ic-openlabs.huawei.com/openlab/api/v1/blue-area-server/pm/update?timestamp='
                headers = {
                        'Referer': 'https://ic-openlabs.huawei.com/',
                        'X-CSRF-TOKEN': csrf,
                        'Content-Type': 'application/json'
                }
                result_list = '{"list": [%s]}' % json.dumps(result)
                resp = s.post(url + timestamp, data=self.__encrypt(result_list, timestamp), headers=headers)
                code, body = resp.status_code, json.loads(self.__decrypt(resp.text, timestamp))
                s.close()
                return [code, str(body)]

