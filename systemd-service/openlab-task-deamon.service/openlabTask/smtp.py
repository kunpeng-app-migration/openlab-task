from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.header import Header
import smtplib


class SMTP:
    def __init__(self):
        with open('/etc/mail.rc', 'r') as fp:
            lines = fp.readlines()

        self.host = ''
        self.port = 65535
        self.user = ''
        self.passwd = ''
        self.sender = ''
        for line in lines:
            if line.startswith('set smtp=smtps://'):
                self.host = line.split(':')[1][2:].strip()
                self.port = int(line.split(':')[-1].strip())
            if line.startswith('set smtp-auth-user='):
                self.user = line.split('=')[-1].strip()
            if line.startswith('set smtp-auth-password='):
                self.passwd = line.split('=')[-1].strip()
            if line.startswith('set from='):
                self.sender = line.split('=')[-1].strip()
        self.server = smtplib.SMTP_SSL(self.host, self.port)
        self.reconnect()

    def reconnect(self):
        self.server.login(self.user, self.passwd)

    def send_email(self, subject, msg):
        message = MIMEText(msg, 'plain', 'utf-8')
        message['From'] = self.sender
        receiver = ['zhouxiaowei13@huawei.com']
        message['To'] = ','.join(receiver)
        message['Subject'] = subject

        return self.send_out(receiver, message)

    def send_out(self, receiver, message):
        try:
            receiver = ['zhouxiaowei13@huawei.com']
            self.server.sendmail(self.sender, receiver, message.as_string())
            return True
        except smtplib.SMTPException as e:
            self.reconnect()
            try:
                self.server.sendmail(self.sender, receiver, message.as_string())
                return True
            except smtplib.SMTPException as e:
                # retry failed, give up
                print(f'Send {message["Subject"]} failed due to {e}')
                pass
        return False

    def quit(self):
        self.server.quit()

