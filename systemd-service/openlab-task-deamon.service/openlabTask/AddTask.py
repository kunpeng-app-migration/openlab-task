#!/usr/bin/env python3
#-*- coding:utf-8 -*-

from openlabTask.TaskCommon import TaskCommon


class AddTask(TaskCommon):
        """
        AddTask Class

        1. Process task which status is prepared or recycling
        2. Call add-pxe-task
        3. Set status to be task_added
        """

        def __init__(self):
                super().__init__()
                self.__add_task()

        def __add_task(self):
                update = False
                for task in self.task_list:
                        system_task_info = self.read_yaml(task)
                        if not system_task_info:
                                continue
                        status = system_task_info['status']
                        if 'vmBase' in system_task_info:
                                continue
                        if status == 'prepared':
                                host = system_task_info['hostName']
                                cblr_profile = system_task_info['cblrProfile']
                                self.log('AddTask', 'host: %s cblr-profile: %s' % (host, cblr_profile))
                                res = self.execute_cmd("add-pxe-task %s %s" % (host, cblr_profile))
                                self.log('AddTask', 'add-pxe-task return:\n' + res)
                                system_task_info['status'] = 'task_added'
                                self.save_yaml(system_task_info, task)
                                update = True
                if update:
                        res = self.execute_cmd('cblr-sync')
                        self.log('AddTask', 'cobbler sync return:\n' + res)

