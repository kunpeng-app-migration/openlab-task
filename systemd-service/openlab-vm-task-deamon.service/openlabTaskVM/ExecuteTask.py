#!/usr/bin/env python3
#-*- coding:utf-8 -*-

import ipaddress
import time
import os
import random
import string

from openlabTaskVM.TaskCommon import TaskCommon
from openlabTaskVM.WebInteract import WebInteract


class ExecuteTask(TaskCommon):
        """
        ExecuteTask Class

        1. Process task which need to CREATE or DELETE
        2. Call deploy_kvm or remove_kvm
        3. Obtain result and post back to web
        """

        def __init__(self):
                super().__init__()
                self.__ip_start = ipaddress.ip_address('192.18.9.101')
                self.__ip_end = ipaddress.ip_address('192.18.9.251')
                self.__add_task()


        def __add_task(self):
                self.task_list.sort()
                for task in self.task_list:
                        system_task_info = self.read_yaml(task)
                        if not isinstance(system_task_info, dict):
                                continue
                        if 'vmImage' not in system_task_info:
                                continue
                        status = system_task_info['status']
                        operation = system_task_info['operation']
                        with open('/srv/openlab-task/vms', 'r') as fp:
                                block = fp.read()
                                fp.seek(0)
                                lines = fp.readlines()
                        kvm = ''.join(list(filter(lambda ch: ch in '0123456789', system_task_info['serverName'])))
                        if status == 'prepared' and operation == 'CREATE':
                                server_weight = {
                                                #'n9-2': 0,
                                                'n9-6': 0,
                                                'n9-8': 0,
                                                #'n9-4': 0
                                                }
                                for line in lines:
                                        tmp_server = line.split()[2]
                                        server_weight[tmp_server] = server_weight.get(tmp_server, 0) + 1
                                min_weight = 1000
                                server = 'n9-2'
                                for tmp_server, weight in server_weight.items():
                                        if weight < min_weight:
                                                server = tmp_server
                                                min_weight = weight
                                vm_base = system_task_info['vmBase']
                                vm_image = system_task_info['vmImage']
                                self_defined_ports = system_task_info['selfDefinedPorts']
                                host = self.__ip_start
                                while host < self.__ip_end:
                                        if str(host) not in block:
                                                break
                                        host += 1
                                host = str(host)
                                system_task_info['serverIp'] = host
                                system_task_info['serviceIp'] = host
                                system_task_info['manageIp'] = host
                                system_task_info['loginIp'] = self.execute_cmd("grep %s %s | awk '{print $2,$3;exit}'" \
                                                      % (host, self.tbox_ssh_ports_file)).strip('\n')
                                system_task_info['selfDefinedPorts'] = self.execute_cmd("grep %s %s | awk '{print $2,$3;exit}'" \
                                                      % (host, self.tbox_service_ports_file)).strip('\n')
                                self.log('ExecuteTask', 'host: %s os: %s' % (host, vm_base))
                                passwd = system_task_info['rootPassword']
                                res = self.execute_cmd("/usr/local/bin/deploy_kvm %s %s %s %s %s '%s'" % (server, kvm, host, vm_base, vm_image, passwd))
                                self.log('ExecuteTask', 'deploy kvm return:\n' + res)
                                if 'KVM created' in res:
                                        system_task_info['status'] = 'allocated'
                                        system_task_info['result'] = 'Success'
                                        system_task_info['msg'] = '发放成功'
                                        result = WebInteract(self.web_key).post_result(system_task_info)
                                        self.log('ExecuteTask', str(system_task_info))
                                        self.log('ExecuteTask', 'Post result:\n%s' % result)
                                        #self.execute_cmd("ssh %s 'echo %s > /root/self_defined_ports.txt'" % (host, self_defined_ports))
                                else:
                                        system_task_info['result'] = 'Fail'
                                        system_task_info['msg'] = '失败'
                                        result = WebInteract(self.web_key).post_result(system_task_info)
                                        self.log('ExecuteTask', str(system_task_info))
                                        self.log('ExecuteTask', 'Post result:\n%s' % result)
                        elif operation == 'DELETE' and status != 'recycled':
                                server = "n9-2"
                                for line in lines:
                                        if kvm in line:
                                                server = line.split()[2]
                                self.log('DeleteKVM', 'host: %s vm: %s' % (server, kvm))
                                res = self.execute_cmd("/usr/local/bin/remove_kvm %s %s" % (server, kvm))
                                if 'removed' in res:
                                        system_task_info['status'] = 'recycled'
                                        system_task_info['result'] = 'Success'
                                        system_task_info['msg'] = '环境回收成功'
                                        result = WebInteract(self.web_key).post_result(system_task_info)
                                        self.log('ExecuteTask', str(system_task_info))
                                        self.log('ExecuteTask', 'Post result:\n%s' % result)
                                else:
                                        system_task_info['result'] = 'Fail'
                                        system_task_info['msg'] = '失败'
                                        system_task_info['status'] = 'recycled'
                                        result = WebInteract(self.web_key).post_result(system_task_info)
                                        self.log('ExecuteTask', str(system_task_info))
                                        self.log('ExecuteTask', 'Post result:\n%s' % result)
                        self.save_yaml(system_task_info, task)

