#!/usr/bin/env python3
#-*- coding:utf-8 -*-

from openlabTaskVM.TaskCommon import TaskCommon
from openlabTaskVM.WebInteract import WebInteract


class AddTask(TaskCommon):
        """
        AddTask Class

        1. Process task which status is prepared or recycling
        2. Call add-pxe-task
        3. Set status to be task_added
        """

        def __init__(self):
                super().__init__()
                self.__add_task()

        def __add_task(self):
                for task in self.task_list:
                        system_task_info = self.read_yaml(task)
                        if 'cblrImage' not in system_task_info:
                                continue
                        status = system_task_info['status']
                        operation = system_task_info['operation']
                        server = 'n9-2'
                        kvm = ''.join(list(filter(lambda ch: ch in '0123456789', system_task_info['serverName'])))
                        if status == 'prepared' and operation == 'CREATE':
                                cblr_profile = system_task_info['cblrProfile']
                                cblr_image = system_task_info['cblrImage']
                                host = system_task_info['serverIp']
                                self.log('AddTask', 'host: %s os: %s' % (host, cblr_profile))
                                res = self.execute_cmd("deploy_kvm %s %s %s %s %s" % (server, kvm, host, cblr_profile, cblr_image))
                                self.log('AddTask', 'add-pxe-task return:\n' + res)
                                if 'KVM created' in res:
                                        system_task_info['status'] = 'allocated'
                                        system_task_info['result'] = 'Success'
                                        system_task_info['msg'] = '发放成功'
                                        result = WebInteract(self.web_key).post_result(system_task_info)
                                        self.log('AddTask', 'Post result:\n%s' % result)
                        elif operation == 'DELETE' and status != 'recycled':
                                self.log('DeleteKVM', 'host: %s vm: %s' % (server, kvm))
                                res = self.execute_cmd("remove_kvm %s %s" % (server, kvm))
                                if 'removed' in res:
                                        system_task_info['status'] = 'recycled'
                                        system_task_info['msg'] = '环境回收成功'
                                        system_task_info['result'] = 'Success'
                                        result = WebInteract(self.web_key).post_result(system_task_info)
                                        self.log('AddTask', 'Post result:\n%s' % result)
                        self.save_yaml(system_task_info, task)

