#!/usr/bin/env python3
#-*- coding:utf-8 -*-
#
# preset file: 
#    1. '/srv/openlab-task/rnd-web-key'
#       from: https://gitee.com/kunpeng-app-migration/security/blob/master/openlab/rnd-web-key

import subprocess
import logging
import time
import yaml
import os


class TaskCommon:
        """
        TaskCommon Class

        1. Initialize task workspace
        2. Read task common config
        3. Define common functions
        """

        def __init__(self):
                self.workspace = '/srv/openlab-task'
                self.web_key_file = '/srv/openlab-task/rnd-web-key' #preset file
                self.tbox_ssh_ports_file = os.path.join(self.workspace, 'tbox-ssh-ports')
                self.tbox_service_ports_file = os.path.join(self.workspace, 'tbox-service-ports')
                self.log_path = os.path.join(self.workspace, 'logs')
                self.task_path = os.path.join(self.workspace, 'tasks')

                # init config
                self.__os_mapping()
                self.__zone_mapping()
                self.__get_task()
                self.__get_web_key()

        def __os_mapping(self):
                self.support_os = {
                        'CentOS 7.6 for ARM': 'CentOS aarch64 7.6.1810',
                        'openEuler 20.03 LTS for ARM': 'openEuler aarch64 20.03 (LTS)',
                        'Ubuntu 18.04.1 LTS for ARM': 'Ubuntu aarch64 18.04.1',
                        'Kylin Linux Advanced Server V10 for ARM': 'Kylin V10 aarch64',
                        'UOS Server 20 Euler(1000) for ARM': 'uos Euler 1010 aarch64',
                        'UOS Server 20 Euler(1010) for ARM': 'uos Euler 1010 aarch64',
                        'NeoKylin Server V7.0 U6 for ARM': 'NeoKylin V7 U6 aarch64',
                        'CentOS 8.1 for ARM': 'CentOS aarch64 8.1.1911'
                     }
                self.vm_base = {
                        'CentOS aarch64 7.6.1810': 'CentOS-7.6.1810-aarch64-base',
                        'openEuler aarch64 20.03 (LTS)': 'openEuler-20.03-aarch64-base',
                        'Ubuntu aarch64 18.04.1': 'Ubuntu-18.04.1-aarch64-base',
                        'Kylin V10 aarch64': 'Kylin-10-SP1-aarch64-base',
                        'uos Euler 1010 aarch64': 'UOS-V20-Euler-1010-aarch64-base',
                        'NeoKylin V7 U6 aarch64': 'NeoKylin-V7-u6-aarch64-base',
                        'CentOS aarch64 8.1.1911': 'CentOS-8.1.1911-aarch64-base'
                     }
                self.vm_img = {
                        'CentOS aarch64 7.6.1810': '1.img',
                        'openEuler aarch64 20.03 (LTS)': '2.img',
                        'Ubuntu aarch64 18.04.1': '3.img',
                        'Kylin V10 aarch64': '4.img',
                        'uos Euler 1010 aarch64': '5.img',
                        'NeoKylin V7 U6 aarch64': '6.img',
                        'CentOS aarch64 8.1.1911': '7.img'
                }

        def __zone_mapping(self):
                self.support_zone = {
                        'BA1': ['Hangzhou-Z9', 'N']
                     }

        def __get_task(self):
                self.task_list = self.execute_cmd("find %s -type f | grep -v -E 'swp|.log|tbox|servers' | sort -r" \
                                                        % self.task_path).split('\n')

        def __get_web_key(self):
                self.web_key = self.execute_cmd('cat %s' % self.web_key_file).split('\n')[0]

        def execute_cmd(self, cmd):
            sub = subprocess.Popen(
                      args=cmd,
                      shell=True,
                      stdout=subprocess.PIPE,
                      stderr=subprocess.STDOUT
                     )
            sub.wait()
            return sub.stdout.read().decode(encoding='utf-8', errors='strict').strip()
        
        def log(self, module, msg):
                date = str(time.strftime('%Y%m%d', time.localtime(time.time())))
                fpath = os.path.join(self.log_path, date, 'vm_all.log')
                self.execute_cmd('mkdir -p $(dirname %s)' % fpath)
                logging.basicConfig(filename=fpath,
                                    level=logging.INFO,
                                    format="[%(asctime)s] %(levelname)s: %(message)s")
                logging.info('[VM][%s]: %s' % (module, msg))
        
        def read_yaml(self, dstfile):
                with open(dstfile, 'r', encoding='utf-8') as f:
                        return yaml.safe_load(f.read())

        def save_yaml(self, data, dstfile):
                with open(dstfile, 'w') as f:
                        yaml.dump(data, f, encoding="utf-8", allow_unicode=True)

