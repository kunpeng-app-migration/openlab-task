#!/usr/bin/env python3
#-*- coding:utf-8 -*-

import os
import time

from openlabTaskVM.WebInteract import WebInteract
from openlabTaskVM.TaskCommon import TaskCommon


class GetTask(TaskCommon):
        """
        GetTask Class

        1. get taskId from web_task and back_task_list
        2. check whether the taskId in back_task_list;
           1.1 if in: check operation and status
                1.1.0 get operation from web_task, status from back_task
                1.1.1 if CREATE: continue
                1.1.2 elif DELETE and allocated: create task
                1.1.2 else: continue

           1.2 else: check zone
                1.2.0 get zone from web_task and supported_zones
                1.2.1 if not supported: pass
                1.2.2 else: check operation
                        1.2.2.1 if CREATE or DELETE: set status prepared, create task
                        1.2.2.2 else: continue
        """

        def __init__(self):
                super().__init__()
                self.__get_new_task()

        # Find the old task path
        # task_id: the new task from Web
        # self.task_list: tasks already exiting under jobs layout
        # return:
        #   None     when task_id new
        #   old_task when task_id old
        def __find_task(self, task_id):
                for old_task in self.task_list:
                        if task_id in str(old_task):
                                return old_task
                return None

        # Check if the web_task is new
        # task_id:   web_task_id
        # operation: web_task_operation
        # zone:      web_task_zone
        # return:
        #   True:  when web_task is new
        #   False: when web_task is old and status not expected
        def __check_new_task(self, task_id, operation, zone):
                old_task = self.__find_task(task_id)
                # found no taskId existing
                if old_task is None:
                        if zone not in self.support_zone.keys():
                                return False
                        elif operation == 'CREATE' or operation == 'DELETE':
                                self.log('GetTask', 'Find no taskId existing, operation ' + operation)
                                return True
                        else:
                                return False
                # found taskId existing
                else:
                        status = self.read_yaml(old_task).get('status')
                        if ( operation == 'CREATE' and ( status == 'old_os_restarted' or \
                                                         status == 'unexpected_os' ) )   \
                           or \
                           ( operation == 'DELETE' and status == 'allocated' ):
                                self.log('GetTask', 'Find taskId %s existing, operation %s status %s' \
                                                                 % (task_id, operation, status) )
                                return True
                        else:
                                return False

        def __get_new_task(self):
                web_task_list = WebInteract(self.web_key).get_list()
                for web_task in web_task_list:
                        task_id = web_task.get('taskId')
                        operation = web_task.get('operation')
                        server_name = web_task.get('serverName')
                        bmc_ip = web_task.get('bmcIp')
                        server_split = server_name.split("_")
                        zone = server_split[0]
                        if not self.__check_new_task('-'.join((task_id, server_name)), operation, zone):
                                continue

                        status = 'prepared'
                        column = self.support_zone[zone][1] 
                        chassis = int(server_split[1].lstrip('0'))
                        slot = int(server_split[2].lstrip('0'))
                        host = "%s%s-%s" % (column, chassis, slot)
                        bmc_host = 'bmc' + host
                        manage_ip = bmc_ip
                        server_ip = bmc_ip
                        login_ip  = self.execute_cmd("grep %s %s | awk '{print $2,$3;exit}'" \
                                                      % (manage_ip, self.tbox_ssh_ports_file)).strip('\n')
                        self_define_ports = self.execute_cmd("grep %s %s | awk '{print $2,$3;exit}'" \
                                                      % (manage_ip, self.tbox_service_ports_file)).strip('\n')
                        set_os = web_task.get('os')
                        vm_base = self.vm_base[self.support_os[set_os]]
                        vm_image = self.vm_img[self.support_os[set_os]]
                        applicant = web_task.get('applicantId')
                        if applicant is None:
                                applicant = ''
                        system_task_info = {
                                'taskId': task_id,
                                'serverName': server_name,
                                'zone': self.support_zone[zone][0],
                                'operation': operation,
                                'applicant': applicant,
                                'status': status,
                                'vmBase': vm_base,
                                'vmImage': vm_image,
                                'position': host,
                                'bmcIp': server_ip,
                                'bmcHostName': bmc_host,
                                'serverIp': server_ip,
                                'manageIp': manage_ip,
                                'serviceIp': manage_ip,
                                'hostName': host,
                                'rootPassword': web_task.get('osPass'),
                                'loginIp': login_ip,
                                'selfDefinedPorts': self_define_ports,
                                'loginPassword': '',
                                'loginAccount': '',
                                'jumperIp': '',
                                'jumperName': '',
                                'msg': '',
                                'result': ''
                             }
                        self.log('GetTask', 'Creating task %s: %s %s' % (task_id, host, vm_base))
                        cur_path = os.path.join(self.task_path, str(time.strftime('%Y%m%d', \
                                                                    time.localtime(time.time()))))
                        self.execute_cmd('mkdir -p %s' % cur_path)
                        self.save_yaml(system_task_info, os.path.join(cur_path, '-'.join((task_id, server_name))))

