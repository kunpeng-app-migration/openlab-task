# openlab-task

#### 介绍
openLab 自动化装机任务脚本仓

#### 软件架构

本服务基本上完成功能解耦，形成流水线工作：  
```
           +~~~~~~~~~~~~~~~~~~~~+
 +---------| current task status|------+
 |         +~~~~~~~~~~~~~~~~~~~~+      |
 |           |           |             |
GetTask -> AddTask -> StartTask -> UpdateTask
 ^                                     |
 |                                     |
 +-------------------------------------+
```
每个模块完成自己的工作，是否工作根据各任务的当前状态。  

#### 安装教程

在PXE服务器上运行`start`脚本，完成服务安装并运行。  
```
s pxe # login pxe-server
cd /srv
git clone https://gitee.com/kunpeng-app-migration/openlab-task
cd openlab-task/systemd-service/openlab-task-deamon.service
./start
```

#### 使用说明

1. `disable`脚本可以停止并删除服务。  
2. `show-openlab-task`脚本可以手动调试，获取前台任务列表及通知结果到前台。
3. `openlabTask` module是依赖的工作流类定义。
4. 服务会开机自启动，持续运行消费前台任务。正常不需要维护。
5. 执行的任务在/srv/openlab-task/tasks/
6. 执行任务日志在/srv/openlab-task/logs/
7. 以下文件需要置于本项目根目录下以保证正常运行：
```shell
rnd-web-key
servers
tbox-ssh-ports
tbox-service-ports
vms
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
